const {
	ContentState,
	ContentBlock,
	convertFromHTML,
	convertToRaw,
	BlockMapBuilder,
	Modifier,
	SelectionState,
	genKey,
	CharacterMetadata
} = require('draft-js');
const {
	List,
	Map,
	Repeat
} = require('immutable');
const initVirtualDOM = require('./modules/jsdom.js');
const args = process.argv.slice(2)[0];
const invariant = require('fbjs/lib/invariant');

initVirtualDOM();

function getSafeBodyFromHTML(html) {
	let doc;
	let root = null;
	// Provides a safe context
	{
		doc = document.implementation.createHTMLDocument('foo');
		invariant(doc.documentElement, 'Missing doc.documentElement');
		doc.documentElement.innerHTML = html;
		root = doc.getElementsByTagName('body')[0];
	}
	if (!root) {
		return null;
	}
	const iframes = root.querySelectorAll('iframe');
	for (let i = 0, length = iframes.length; i < length; i++) {
		if (iframes[i].hasAttribute('src')) {
			const src = iframes[i].getAttribute('src');
			const iframePlaceholder = document.createElement('a');
			iframePlaceholder.setAttribute('rel', 'umedia-iframe-placeholder ' + src);
			iframePlaceholder.setAttribute('href', 'https://u.media');
			iframePlaceholder.textContent = 'umedia-iframe-placeholder';
			iframes[i].parentNode.replaceChild(iframePlaceholder, iframes[i]);
		}
	}
	return root;
}

try {
	const {
		contentBlocks,
		entityMap
	} = convertFromHTML(args, getSafeBodyFromHTML);
	let contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
	let currentBlock = contentState.getFirstBlock();
	let selectionState = null;
	
	const infinityLoop = (number) => {
		let counter = 0;
	
		return () => {
	
			if (counter >= number) throw new Error('Reach Max Loop');
	
			counter++;
		}
	};
	
	const guard = infinityLoop(1000);
	const getEntity = (char) => {
		const entityKey = char.getEntity();
		if (!entityKey) return null;
		return contentState.getEntity(entityKey);
	}
	const isImage = entity => entity && entity.type === 'IMAGE';
	const isIframe = entity => entity && entity.type === 'LINK' && entity.getData().rel && entity.getData().rel.includes('umedia-iframe-placeholder');
	const emptyBlock = () => new ContentBlock({
		key: genKey(),
		type: 'unstyled',
		data: Map({
			isTemporaryPlaceholder: true
		}),
	});
	const imageBlock = entity => {
		const character = '';
		const charData = CharacterMetadata.create();
	
		return new ContentBlock({
			key: genKey(),
			type: 'atomic',
			text: character,
			characterList: List(Repeat(charData, character.length)),
			data: Map({
				src: entity.getData().src,
				isExternal: true,
				loadByURL: true,
				type: 'image-external',
			})
		});
	}
	const iframeBlock = entity => {
		const character = '';
		const charData = CharacterMetadata.create();
		const src = entity.getData().rel.replace('umedia-iframe-placeholder ', '');
		return new ContentBlock({
			key: genKey(),
			type: 'atomic',
			text: character,
			characterList: List(Repeat(charData, character.length)),
			data: Map({
				url: src,
				html: "<iframe frameborder=\"0\" src=" + src + "/>",
				type: "embedded",
				embedType: "",
				thumbnailUrl: "",
				thumbnailWidth: 0,
				wrapWithIframe: false,
				thumbnailHeight: 0,
			}),
		});
	};
	
	while (currentBlock) {
		guard();
		let found = false;
	
		currentBlock.findEntityRanges((char) => isImage(getEntity(char)) || isIframe(getEntity(char)), (start, end) => {
			if (found) {
				return;
			}
			selectionState = new SelectionState({
				anchorKey: currentBlock.getKey(),
				focusKey: currentBlock.getKey(),
				anchorOffset: start,
				focusOffset: end
			});
			const entity = contentState.getEntity(currentBlock.getEntityAt(start));
			const block = isImage(entity) ? imageBlock(entity) : iframeBlock(entity);
			const fragment = BlockMapBuilder.createFromArray([emptyBlock(), block, emptyBlock()]);
	
			const afterRemoval = Modifier.removeRange(
				contentState,
				selectionState,
				'backward',
			);
	
			const targetSelection = afterRemoval.getSelectionAfter();
			const afterSplit = Modifier.splitBlock(afterRemoval, targetSelection);
			const insertionTarget = afterSplit.getSelectionAfter();
	
			const asAtomicBlock = Modifier.setBlockType(
				afterSplit,
				insertionTarget,
				'atomic',
			);
	
			const withAtomicBlock = Modifier.replaceWithFragment(
				asAtomicBlock,
				insertionTarget,
				fragment,
			);
	
			contentState = withAtomicBlock.merge({
				selectionBefore: selectionState,
				selectionAfter: withAtomicBlock.getSelectionAfter(),
			});
	
			selectionState = contentState.getSelectionAfter();
	
			found = true;
		});
	
		if (found) {
			currentBlock = contentState.getBlockForKey(selectionState.getAnchorKey());
			found = false;
			continue;
		}
	
		if (currentBlock.getKey() !== contentState.getLastBlock().getKey()) {
			currentBlock = contentState.getBlockAfter(currentBlock.getKey());
			continue;
		}
	
		break;
	}
	
	const data = convertToRaw(contentState);
	data.blocks = data.blocks
		.filter((block) => !(block.data.isTemporaryPlaceholder && block.text.length === 0))
		.map(block => {
			if (block.data.isTemporaryPlaceholder) {
				delete block.data.isTemporaryPlaceholder;
			}
			return block;
		});

		process.stdout.write(JSON.stringify(data));
	
} catch (error) {
	process.stderr.write(error.message);
}
