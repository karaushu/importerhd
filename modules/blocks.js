const IMAGE_EMOJI = '📷';
const uuid = require('uuid');
const MUTATED = [];
let imagesMap = new Map();

const replaceBlocks = (blocks, images) => {
  imagesMap = images;
  blocks.map((block) => {
    if (isContainingImageEntity(block)) {
      const entityImages = getImageEntities(block);
  
      if (isSingleImage(block, entityImages)) {
        setBlockData(block, getImageFromMap(entityImages[0].key.toString()));
        MUTATED.push(block);
      } else if (entityImages.length > 0) {
        mutateBlock(block, entityImages);
      } else {
        MUTATED.push(block);
      }
    } else {
      MUTATED.push(block);
    }
  });

  return MUTATED;
};

let prevOffset = 0;
const mutateBlock = (block, entityImages) => {
  block.text = block.text.replace(IMAGE_EMOJI, '');
  const notImagesEntities = getNotImageEntities(block);
  entityImages.map((entity, index) => {
    setNewBlockFromEntity(block, entity, entityImages.length === index+1, notImagesEntities);
  });
  prevOffset = 0;
}

const setNewBlockFromEntity = (block, entity, isLast, notImagesEntities) => {
  const newInlineStyleRanges = block.inlineStyleRanges.filter((style) => style.offset <= entity.offset);

  if (newInlineStyleRanges.length > 0)
    block.inlineStyleRanges = [...block.inlineStyleRanges.filter((style) => !newInlineStyleRanges.includes(style))];

  const newBlock = {};
  setBlockData(newBlock, getImageFromMap(entity.key.toString()));

  if (entity.offset > 0) {
    const newTextBlock = {};
    const text = block.text.slice(prevOffset, entity.offset).replace(IMAGE_EMOJI, '');
    if (text && text.length > 0) {
      const newInlineStyleRangesWithOffset = newInlineStyleRanges.map((style) => {
        style.offset -= prevOffset;
        return style;
      });
      const newEntitiesRangeWithOffset = notImagesEntities.filter((e) => e.offset <= entity.offset).map((e) => {
        e.offset -= prevOffset;
        return e;
      });
      setTextData(newTextBlock, text, newInlineStyleRangesWithOffset, newEntitiesRangeWithOffset);
      MUTATED.push(newTextBlock);
    }
    if (isLast && block.text.slice(entity.offset, block.text.length).length > 0) {
      const newInlineStyleRangesWithOffset = block.inlineStyleRanges.filter((style) => style.offset > entity.offset).map((style) => {
        style.offset -= entity.offset;
        return style;
      });
      const newEntitiesRangeWithOffset = notImagesEntities.filter((e) => e.offset > entity.offset).map((e) => {
        e.offset -= entity.offset;
        return e;
      });
      const lastBlock = {};
      setTextData(lastBlock, block.text.slice(entity.offset, block.text.length).replace(IMAGE_EMOJI, ''), newInlineStyleRangesWithOffset, newEntitiesRangeWithOffset);
      MUTATED.push(lastBlock);
    }
    MUTATED.push(newBlock);
  } else {
    MUTATED.push(newBlock);
  }
  
  prevOffset = entity.offset;
  console.log(newInlineStyleRanges, block.inlineStyleRanges);
}

const setBlockData = (block, image) => {
  block.type = 'atomic';
  block.text = '';
  block.data = {
    "src": image,
    "type": "image-external",
    "loadByURL": true,
    "isExternal": true
  };
  block.depth = 0;
  block.inlineStyleRanges = [];
  block.entityRanges = [];

  setBlockKey(block);
}

const setTextData = (block, text, inlineStyleRanges, entityRanges) => {
  block.type = 'unstyled';
  block.text = text;
  block.depth = 0;
  block.inlineStyleRanges = inlineStyleRanges;
  block.entityRanges = entityRanges;
  block.data = {};

  setBlockKey(block);
}

const setBlockKey = (block) => {
  if (block.key && block.key.length > 0) return false;

  block.key = uuid();
}

const getImageFromMap = (key) => {
  return imagesMap.get(key);
}

const isContainingImageEntity = (block) => {
  return block.entityRanges && block.entityRanges.length > 0;
}

const isSingleImage = (block, entityImages) => {
  return entityImages.length === 1 && block.text === IMAGE_EMOJI;
}

const getImageEntities = (block) => {
  return block.entityRanges.filter(entity => imagesMap.has(entity.key.toString()));
}

const getNotImageEntities = (block) => {
  return block.entityRanges.filter(entity => !imagesMap.has(entity.key.toString()));
}

module.exports = replaceBlocks;
