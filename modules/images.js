const IMAGE = 'IMAGE';

const getImagesFromEntities = (entityMap) => {
  const images = new Map();
  const newEntityMap = {};
  
  for (const [key, value] of Object.entries(entityMap)) {
    if (isImage(value)) {
      images.set(key, value.data.src);
    } else {
      newEntityMap[key] = value;
    }
  }

  return {
    imageMap: images,
    entityMap: newEntityMap
  };
}

const isImage = (entity) => {
  return entity.type === IMAGE;
}

module.exports = getImagesFromEntities;