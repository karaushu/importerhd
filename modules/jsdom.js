const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const { window } = new JSDOM();

function virtualDOM() {
	global.window = window;
	global.document = window.document;
	global.navigator = window.navigator;
	global.HTMLElement = window.HTMLElement;
	global.HTMLImageElement = window.HTMLImageElement;
	global.HTMLAnchorElement = window.HTMLAnchorElement;
	global.HTMLBRElement = window.HTMLBRElement;
}

module.exports = virtualDOM;